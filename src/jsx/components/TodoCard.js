import React from 'react';

function TodoCard(props) {
    return(
        <React.Fragment>
        <div id='izq_card'></div>
        <div id='center_card'>
            <div id="card">
                {props.children}
            </div>
        </div>
        <div id='der_card'></div>
        </React.Fragment>
    );
}

export {TodoCard};