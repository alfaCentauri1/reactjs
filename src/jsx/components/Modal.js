import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import '../../css/Modal.css';

function Modal({ children }) {
  return( 
    <Fragment>
      ReactDOM.createPortal(
        <div className="ModalBackground">
          {children}
        </div>,
        document.getElementById('modal')
      )
    </Fragment>    
  );
}

export { Modal };
