import React from "react";
import '../../css/TodoSearch.css';
import { TodoContext } from "../Contexts/TodoContext";

function TodoSearch() {
    const {
        searchValue,
        setSearchValue,
      } = React.useContext(TodoContext);
    return(
        <form id='formulario'>
            <input type="text" id="search" 
                placeholder="Buscar una tarea" 
                className="TodoSearch" 
                value={searchValue}
                onChange={(event) => {
                    console.log("Escribiste en el input");
                    setSearchValue(event.target.value);
                    console.log(event.target.value);
                    } 
                }
            />           
        </form>
    );
}

export { TodoSearch };