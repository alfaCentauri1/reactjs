import React from "react";
import '../../css/TodoForm.css';
import { TodoContext } from '../Contexts/TodoContext.js';

function TodoForm() {
    //Para utilizar el contexto
    const {
        addTodo,
        setOpenModal,
    } = React.useContext(TodoContext);
    //Estado local, almacena temporalmente el valor escrito en el textArea
    const [newTodoValue, setNewTodoValue] = React.useState('');
    //Para enviar el texto al contexto y cerrar el modal
    const onSubmit = (event) => {
        event.preventDefault();
        addTodo(newTodoValue);
        setOpenModal(false);
    };
    //Para cerrar el modal
    const onCancel = () => {
        setOpenModal(false);
    };
    //Para actualizar el estado local
    const onChange = (event) => {
        setNewTodoValue(event.target.value);
    };
    //Estructura del html
    return (
        <form onSubmit={onSubmit}>
        <label>Esribe tu nuevo TODO</label>
        <textarea
            placeholder="Escriba una nueva tarea"
            value={newTodoValue}
            onChange={onChange}
        />
        <div className="TodoForm-buttonContainer">
            <button
            type="button"
            className="TodoForm-button TodoForm-button--cancel"
            onClick={onCancel}
            >Cancelar</button>
            <button
            type="submit"
            className="TodoForm-button TodoForm-button--add"
            >Añadir</button>
        </div>
        </form>
    );
}

export { TodoForm };
