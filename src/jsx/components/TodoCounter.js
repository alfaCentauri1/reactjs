import React from 'react';
import { TodoContext } from '../Contexts/TodoContext.js';
import '../../css/TodoCounter.css';

function TodoCounter() {
    const {
        completedTodos,
        totalTodos,
    } = React.useContext(TodoContext);
    return (
      <h1>Titulo: Terminadas {completedTodos} de {totalTodos} tareas.</h1>
    );
  }
  
  export {TodoCounter};