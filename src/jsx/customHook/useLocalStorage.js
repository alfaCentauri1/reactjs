import React from "react";

function useLocalStorage( itemName, initialValue ) {
  const [item, setItem] = React.useState(initialValue);
  const [loading, setLoading] = React.useState(true);
  const [error, setError] = React.useState(false);
  
  React.useEffect(() => {
    setTimeout(() => {
      try {
        const localStorageItem = localStorage.getItem(itemName);    
        let parsedItem;
        //  
        if (!localStorageItem) {
          localStorage.setItem(itemName, JSON.stringify(initialValue));
          parsedItem = initialValue;
        } else {
          parsedItem = JSON.parse(localStorageItem);
          setItem(parsedItem);
        }   
      } catch(error) {
        setError(true);
      } finally {
        setLoading(false);
      }
    }, []);
  });
    //Guardar cambios en el localstorage
    const saveItem = (newTodos) => {
      localStorage.setItem(itemName, JSON.stringify(newTodos));
      setItem(newTodos);
    };
  
    //Borrar item en local storage
    const deleteItem = (itemName) => {
      localStorage.removeItem(itemName);
    }
  
    //return [item, saveItem, deleteItem];
    return {
      item,
      saveItem,
      deleteItem,
      loading,
      error,
    };
  }
  export {useLocalStorage};