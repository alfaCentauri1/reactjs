import React from 'react';
import { TodoCounter } from '../components/TodoCounter.js';
import { TodoSearch } from '../components/TodoSearch.js';
import { TodoList } from '../components/List.js';
import { TodoItem } from '../components/TodoItem.js';
import { TodosLoading } from '../components/TodosLoading.js';
import { TodosError } from '../components/TodosError.js';
import { EmptyTodos } from '../components/EmptyTodos.js';
import { CreateTodoButton } from '../components/CreateTodoButton.js';
import { TodoContext } from '../Contexts/TodoContext.js';
import { Modal } from '../components/Modal.js';
import { TodoForm } from '../components/TodoForm.js';

/*
const arreglo =[
  {texto: "Leer El código limpio", completed: true},
  {texto: "Leer tutorial de ReactJS", completed: false},
  {texto: "Escribir manual", completed: false}
];
localStorage.setItem('TODOS_V1', JSON.stringify(arreglo) );*/

function AppUI() {
  const {
    loading,
    error,
    searchedTodos,
    completeTodo,
    deleteTodo,
    openModal,
    setOpenModal,
  } = React.useContext(TodoContext);
  
  return (
    <>
      <TodoCounter />
      <TodoSearch />

      <TodoList>
        {loading && (
          <>
            <TodosLoading />
            <TodosLoading />
            <TodosLoading />
          </>
        )}
        {error && <TodosError/>}
        {(!loading && searchedTodos.length === 0) && <EmptyTodos />}

        {searchedTodos.map(todo => (
          <TodoItem
            key={todo.texto}
            texto={todo.texto}
            completed={todo.completed}
            onComplete={() => completeTodo(todo.texto)}
            onDelete={() => deleteTodo(todo.texto)}
          />
        ))}
      </TodoList>
      
      <CreateTodoButton
        setOpenModal={setOpenModal}
      />

      {openModal && (
        <Modal>
          <TodoForm />
        </Modal>
      )}
    </>
  );
}

export { AppUI };
