import { TodoList } from '../components/List';
import { TodoItem } from '../components/TodoItem';
import { TodoCounter } from '../components/TodoCounter';
import { TodoSearch } from '../components/TodoSearch';
import { CreateTodoButton } from '../components/CreateTodoButton';
import React from 'react';
import { useLocalStorage } from '../customHook/useLocalStorage';
import { AppUI } from '../customHook/AppUIHook';

/*
const arreglo =[
  {texto: "Leer El código limpio", completed: true},
  {texto: "Leer tutorial de ReactJS", completed: false},
  {texto: "Leer El Hobbit", completed: true},
  {texto: "Leer El Señor de los Anillos: La comunidad del anillo", completed: true},
  {texto: "Leer El Señor de los Anillos: Las dos torres", completed: true},
  {texto: "Leer El Señor de los Anillos: El retorno del rey", completed: true},
  {texto: "Leer El informe pelicano", completed: false},
  {texto: "Leer Comer, Rezar y Amar", completed: false},
  {texto: "Leer Cien años de soledad", completed: false},
  {texto: "Escribir manual", completed: false}
];
localStorage.setItem('TODOS_V1', JSON.stringify(arreglo) );
*/
//Custom hook: use local storage

//
function App() {
  const {
    item: todos,
    saveItem: saveTodos,
    loading,
    error,
  } = useLocalStorage('TODOS_V1', []);
  const [searchValue, setSearchValue] = React.useState('');

  //Filtrar por tareas completadas
  const completedTodos = todos.filter(
    todo => !!todo.completed
  ).length;
  const totalTodos = todos.length;

  //Filtrar por texto buscado
  const searchedTodos = todos.filter(
    (todo) => {
      const todoText = todo.texto.toLowerCase();
      const searchText = searchValue.toLowerCase();
      return todoText.includes(searchText);
    }
  );

  //Completar la tarea
  const completeTodo = (text) => {
    const newTodos = [...todos];
    const todoIndex = newTodos.findIndex(
      (todo) => todo.texto == text
    );
    newTodos[todoIndex].completed = true;
    saveTodos(newTodos);
  };

  //Borrar la tarea
  const deleteTodo = (text) => {
    const newTodos = [...todos];
    const todoIndex = newTodos.findIndex(
      (todo) => todo.texto == text
    );
    newTodos.splice(todoIndex, 1);
    saveTodos(newTodos);
  };
  
  return (
    <AppUI
      loading={loading}
      error={error}
      completedTodos={completedTodos}
      totalTodos={totalTodos}
      searchValue={searchValue}
      setSearchValue={setSearchValue}
      searchedTodos={searchedTodos}
      completeTodo={completeTodo}
      deleteTodo={deleteTodo}
      setNuevaTarea={setNuevaTarea}
    />
  );
}

export default App;
