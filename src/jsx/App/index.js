import React from 'react';
import { TodoProvider } from '../Contexts/TodoContext.js';
import { AppUI } from '../customHook/AppUIHook.js';

function App() {
  return (
    <TodoProvider>
      <AppUI />
    </TodoProvider>
  );
}

export default App;