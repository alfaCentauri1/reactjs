import React from 'react';
import { useLocalStorage } from './useLocalStorage.js';
//Definición del contexto
const TodoContext = React.createContext();
//Definición del proveedor del contexto
function TodoProvider({ children }) {
    //Objeto para usar el local storage
    const {
        item: todos,
        saveItem: saveTodos,
        loading,
        error,
    } = useLocalStorage('TODOS_V1', []);
    const [searchValue, setSearchValue] = React.useState('');
    const [openModal, setOpenModal] = React.useState(false);
    //Para filtrar
    const completedTodos = todos.filter(
        todo => !!todo.completed
    ).length;
    const totalTodos = todos.length;
    //Para buscar
    const searchedTodos = todos.filter(
        (todo) => {
        const todoText = todo.texto.toLowerCase();
        const searchText = searchValue.toLowerCase();
        return todoText.includes(searchText);
        }
    );
    //Para guardar nuevo
    const addTodo = (texto) => {
        const newTodos = [...todos];
        newTodos.push({
        texto,
        completed: false,
        });
        saveTodos(newTodos);
    };
    //Para la acción de completar
    const completeTodo = (texto) => {
        const newTodos = [...todos];
        const todoIndex = newTodos.findIndex(
        (todo) => todo.texto === texto
        );
        newTodos[todoIndex].completed = true;
        saveTodos(newTodos);
    };
    //Para borrar
    const deleteTodo = (texto) => {
        const newTodos = [...todos];
        const todoIndex = newTodos.findIndex(
        (todo) => todo.texto === texto
        );
        newTodos.splice(todoIndex, 1);
        saveTodos(newTodos);
    };
    //Para publicar y exportar
    return (
        <TodoContext.Provider value={{
        loading,
        error,
        completedTodos,
        totalTodos,
        searchValue,
        setSearchValue,
        searchedTodos,
        addTodo,
        completeTodo,
        deleteTodo,
        openModal,
        setOpenModal,
        }}>
        {children}
        </TodoContext.Provider>
    );
}

export { TodoContext, TodoProvider };
