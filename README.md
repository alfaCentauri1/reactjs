# ReactJS
Ejemplo de React.


## Getting started
Ejemplos básicos de react.js, componentes con props, hooks and context.

## Name
Ejemplos de REACT.js.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Ejecutar en el termianl: npm install

## Usage
Ejecutar en el terminal: npm start

## Authors and acknowledgment
alfaCentauri

## License
GNU version 3.

## Project status
In develop.
